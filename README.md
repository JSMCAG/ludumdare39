# Tank Empty

Driving along the highway, you find yourself low on fuel and in the most treacherous area of the highway.
Collect the gas cans to refill your tank, while avoiding road obstacles and other drivers, to reach as far as you can.

Use the arrow keys or WASD to move. M mutes/unmutes the music. Space to start the game.

Good luck!

## About the development

This is the source code for my entry for the [Ludum Dare 39](https://ldjam.com/events/ludum-dare/39) Compo, following the theme "Rumming out of Power".

It was made in about 6 hours using GameMaker Studio 1.4 (I know I have more time, but life gets in the way, so this will have to do).
The code is rather messy, given it was written in a rush - apologies for those who decide to check it out.